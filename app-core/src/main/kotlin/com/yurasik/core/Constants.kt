package com.yurasik.core

import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

const val DECIMAL_FORMAT_DOT = "##.#"
const val DECIMAL_FORMAT_COMMA = "#,###.#"

const val DATE_STUB = "--/--"
const val TIME_STUB = "--:--"
const val DATE_TIME_STUB = "--/--,--:--"

val dateFormatter = SimpleDateFormat("dd/MM/yyyy")
val timeFormatter = SimpleDateFormat("HH:mm")
val dateTimeFormatter = SimpleDateFormat("dd/MM/yyyy - HH:mm")

fun SimpleDateFormat.formatOrDefaultIfNull(date: Date?, default: String = ""): String = if (date == null) default else format(date)

// Use DecimalFormat without currency sign. It may change dynamically in future
val moneyDecimalFormat = DecimalFormat("'₪'$DECIMAL_FORMAT_COMMA")
val moneyDecimalSpaceFormat = DecimalFormat("$DECIMAL_FORMAT_COMMA '₪'")

val distanceKmDecimalFormat = DecimalFormat("$DECIMAL_FORMAT_DOT 'km'")

