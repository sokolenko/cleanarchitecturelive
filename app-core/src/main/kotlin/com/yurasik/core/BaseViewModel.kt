package com.yurasik.core

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class BaseViewModel : ViewModel() {

    init {
        viewModelScope.launch {

        }

        block {
            println("****************")
        }
    }


}

public fun ViewModel.block(block: suspend CoroutineScope.() -> Unit) {
    viewModelScope.launch(block = block)
}