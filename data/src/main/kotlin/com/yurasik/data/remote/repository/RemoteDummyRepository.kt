package com.yurasik.data.remote.repository

import com.yurasik.domain.entities.DummyPost
import com.yurasik.domain.entities.base.DataEntity
import com.yurasik.domain.entities.base.ErrorEntity
import com.yurasik.domain.repositories.DummyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import java.lang.Exception

class RemoteDummyRepository : DummyRepository {
    override suspend fun getPosts(): ReceiveChannel<DataEntity<DummyPost>> {
        val producer = GlobalScope.produce<DataEntity<DummyPost>> {
            try {
                //val news = api.getNews().await()
                //newsMapper.mapToEntity(news.articles).let { send(DataEntity.SUCCESS(it)) }
                send(DataEntity.SUCCESS(DummyPost("Test post")))
                for (i in 1..100000) {
                    send(DataEntity.SUCCESS(DummyPost("Test post: $i")))
                    delay(1000)
                }
            } catch (e: Exception) {
                send(DataEntity.ERROR(ErrorEntity(e.message)))
            }
        }

        return producer
    }
}