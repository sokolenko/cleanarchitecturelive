package com.yurasik.data.remote

import com.yurasik.data.remote.repository.RemoteDummyRepository
import com.yurasik.data.remote.retrofit.RemoteNewsApi
import com.yurasik.data.remote.retrofit.createGpOkHttpSimpleClient
import com.yurasik.data.remote.retrofit.createRetrofit
import com.yurasik.data.remote.retrofit.createWebService
import com.yurasik.domain.repositories.DummyRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val OKHTTP_SIMPLE = "OKHTTP_SIMPLE"
val RETROFIT_SIMPLE = "RETROFIT_SIMPLE"
val URL = "https://www.reddit.com/"

val remoteModule = module {
    single(named(OKHTTP_SIMPLE)) { createGpOkHttpSimpleClient() }
    single(named(RETROFIT_SIMPLE)) { createRetrofit(get(named(OKHTTP_SIMPLE)), URL) }

    single { createWebService<RemoteNewsApi>(get(named(RETROFIT_SIMPLE))) }

    factory<DummyRepository> { RemoteDummyRepository() }
}