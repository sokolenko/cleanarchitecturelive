package com.yurasik.data.remote.retrofit

import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.http.GET

// https://www.reddit.com/r/all/top/.json?limit=1

interface RemoteNewsApi {

    @GET("r/all/top/.json?limit=1")
    suspend fun getNews(): ResponseBody

}