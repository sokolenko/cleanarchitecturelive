package com.yurasik.appreddit.presentation.mappers

import com.yurasik.core.models.Data
import com.yurasik.domain.common.Mapper
import com.yurasik.domain.entities.DummyPost
import com.yurasik.domain.entities.base.DataEntity

class DummyPostMapper : Mapper<DataEntity<DummyPost>, Data<DummyPost>>() {
    override fun mapFrom(data: DataEntity<DummyPost>): Data<DummyPost> {
        when (data) {
            is DataEntity.SUCCESS -> return@mapFrom Data.SUCCESS(data.data?.let { mapSourcesToPresetation(it) })
            is DataEntity.ERROR -> return@mapFrom  Data.ERROR(error = Error( data.error.message))
            is DataEntity.LOADING -> return@mapFrom  Data.LOADING()
        }
    }

    private fun mapSourcesToPresetation(sources: DummyPost)
            : DummyPost = DummyPost(sources.name)

}