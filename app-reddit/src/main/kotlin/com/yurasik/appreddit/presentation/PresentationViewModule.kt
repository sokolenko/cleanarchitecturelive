package com.yurasik.appreddit.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yurasik.core.block
import com.yurasik.core.models.Data
import com.yurasik.domain.common.Mapper
import com.yurasik.domain.entities.DummyPost
import com.yurasik.domain.entities.base.DataEntity
import com.yurasik.domain.usecases._base.GetPostsUseCase
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

class PresentationViewModule(private val useCase: GetPostsUseCase,
                             private val mapper: Mapper<DataEntity<DummyPost>, Data<DummyPost>>
) : ViewModel() {

    var mNews = MutableLiveData<Data<DummyPost>>()

    fun test() {
        println("Test")
        block {
            val posts = useCase.getPostsData()
            posts.consumeEach {response ->
                val mappedResponse = mapper.mapFrom(response)
                mNews.postValue(mappedResponse)
            }
        }
    }


}