package com.yurasik.appreddit.presentation

import com.yurasik.appreddit.presentation.mappers.DummyPostMapper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel { PresentationViewModule(get(), DummyPostMapper()) }
}