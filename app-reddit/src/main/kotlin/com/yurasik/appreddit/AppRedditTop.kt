package com.yurasik.appreddit

import android.app.Application
import com.yurasik.appreddit.presentation.presentationModule
import com.yurasik.data.remote.remoteModule
import com.yurasik.domain.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AppRedditTop : Application(){
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin{
            androidLogger()
            androidContext(this@AppRedditTop)
            modules(
                listOf(
                    remoteModule,
                    domainModule,
                    presentationModule
                )
            )
        }
    }
}