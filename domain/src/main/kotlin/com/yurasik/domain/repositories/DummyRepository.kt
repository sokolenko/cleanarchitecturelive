package com.yurasik.domain.repositories

import com.yurasik.domain.entities.DummyPost
import com.yurasik.domain.entities.base.DataEntity
import kotlinx.coroutines.channels.ReceiveChannel

interface DummyRepository {

    suspend fun getPosts() : ReceiveChannel<DataEntity<DummyPost>>
}