package com.yurasik.domain

import com.yurasik.domain.usecases._base.GetPostsUseCase
import kotlinx.coroutines.Dispatchers
import org.koin.core.qualifier.named
import org.koin.dsl.module
import kotlin.coroutines.CoroutineContext

const val DEFAULT_COROUTINE_CONTEXT = "DEFAULT_COROUTINE_CONTEXT"

val domainModule = module {
    single<CoroutineContext>(named(DEFAULT_COROUTINE_CONTEXT)) { Dispatchers.Default }

    factory { GetPostsUseCase(get(named(DEFAULT_COROUTINE_CONTEXT)), get()) }
}