package com.yurasik.domain.entities.post

data class RedditPost (
    val key: String,
    val title: String,
    val image: String,
    val author: String,
    val commentCount: Int
)