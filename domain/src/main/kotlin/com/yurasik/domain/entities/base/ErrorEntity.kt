package com.yurasik.domain.entities.base

data class ErrorEntity(var message: String?= null)