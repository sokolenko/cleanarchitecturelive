package com.yurasik.domain.usecases._base

import com.yurasik.domain.entities.DummyPost
import com.yurasik.domain.entities.base.DataEntity
import com.yurasik.domain.repositories.DummyRepository
import kotlinx.coroutines.channels.ReceiveChannel
import kotlin.coroutines.CoroutineContext


class GetPostsUseCase(private val coroutineContext: CoroutineContext,
                      private val repository: DummyRepository) : BaseJobUseCase<DummyPost>(coroutineContext) {
    override suspend fun getDataChannel(data: Map<String, Any>?): ReceiveChannel<DataEntity<DummyPost>> {
        return repository.getPosts()
    }

    override suspend fun sendToPresentation(data: DataEntity<DummyPost>): DataEntity<DummyPost> {
        return data
    }

    suspend fun getPostsData(): ReceiveChannel<DataEntity<DummyPost>> {
        val data = HashMap<String, String>()
        return produce(data)
    }
}